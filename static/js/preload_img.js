
(function($){

	$.fn.preloadimg = function(settings){
	
		var settings = jQuery.extend({
		
			imgnum		: 0  ,
			curnum		: 0  ,		
			pre_imgurl	: ''
		
		}, settings);
		
		var jQueryMatchedObj = this;

		settings.imgnum = $(jQueryMatchedObj).attr("imgnum");
		settings.curnum = 0;
		
		function getImgArr(){
			
			settings.curnum++;

			if(settings.curnum > settings.imgnum) return;	
			
			settings.pre_imgurl = $(jQueryMatchedObj).attr("imgsrc"+settings.curnum);
			
			if ( settings.pre_imgurl == '' || settings.pre_imgurl == undefined) return;

			auto_load_img();

		}
		
		function auto_load_img(){
			
			var objImagePreloader = new Image();
			objImagePreloader.onload = function() {
				
				objImagePreloader.onload=function(){};
				
				if ( typeof preload_debug != "undefined" && preload_debug == 1){
					$("body").append(objImagePreloader);	
				}
				
				
				getImgArr();
	
				
			};
			
			objImagePreloader.onerror = function() {
				
				objImagePreloader.onerror=function(){};
				
				getImgArr();
				
			};
			objImagePreloader.src = settings.pre_imgurl;
		};
		
		function _initialize() {
			settings.curnum = 0;
			getImgArr();
		}
		
		getImgArr();
	
	};

})(jQuery);